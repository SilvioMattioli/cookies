package com.example.layout

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_bestellen.*
import kotlinx.android.synthetic.main.activity_one_product.*
import kotlinx.android.synthetic.main.edit_layout.*
import kotlinx.android.synthetic.main.edit_layout.view.*

class OneProduct : AppCompatActivity(), View.OnClickListener {

    private var coll: CollectionReference? = null
    private val db = FirebaseFirestore.getInstance()
    private var userId: String? = null
    private var docId: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one_product)

        var acct: GoogleSignInAccount = GoogleSignIn.getLastSignedInAccount(this)!!
        userId = acct.id.toString()
        coll = db.collection("Users/$userId/Orders")

        Naam.text = intent.getStringExtra("NAME")
        txt_aantal_een.text = intent.getStringExtra("AANTAL")
        docId = intent.getStringExtra("DOCID")
        afb.setImageResource(intent.getIntExtra("IMAGE", DEFAULT_BUFFER_SIZE))

        btn_delete.setOnClickListener(this)
        btn_edit.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_delete -> delete()
            R.id.btn_edit -> edit()
            else -> println("Number too high")
        }
    }

    private fun delete() {
        coll?.document(docId.toString())!!.delete()
        val i = Intent(this, OverzichtBestellingen::class.java)
        startActivity(i)
    }

    private fun edit() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.edit_layout,null)
        val builder = AlertDialog.Builder(this@OneProduct)

        builder.setTitle("Wilt u deze bestelling aanpassen?")
            .setView(mDialogView)

        val dialog: AlertDialog = builder.create()
        dialog.show()
        dialog.ad_edit.setOnClickListener{
            val number = mDialogView.ad_aantal.text.toString().toInt()
            coll?.document(docId.toString())!!.update("quantity",number )
            val i = Intent(this, OverzichtBestellingen::class.java)
            startActivity(i)
        }
       dialog.ad_cancel.setOnClickListener{dialog.dismiss()

       }


    }


}
