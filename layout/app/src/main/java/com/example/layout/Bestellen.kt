package com.example.layout

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.layout.data.Object
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignIn.getClient
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_bestellen.*
import java.time.LocalDateTime


class Bestellen : AppCompatActivity(), View.OnClickListener, TextWatcher {

    private var mgoogleSingin: GoogleSignInClient? = null
    private var coll: CollectionReference? = null
    private val db = FirebaseFirestore.getInstance()
    private var userId: String? = null
    private var product: String? = null
    private var tot: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bestellen)
        var acct: GoogleSignInAccount = GoogleSignIn.getLastSignedInAccount(this)!!
        userId = acct.id.toString()
        coll = db.collection("Users/$userId/Orders")

        if (acct != null) {

            supportActionBar?.setDisplayShowTitleEnabled(true)
            supportActionBar?.title = acct.displayName
            mgoogleSingin = getClient(this, GoogleSignInOptions.DEFAULT_SIGN_IN)
        }
        var test: Button = findViewById(R.id.btn_bestel)
        var quantity: EditText = findViewById(R.id.txt_aantal)
        var spin: Spinner = findViewById(R.id.spinner1)

        test.setOnClickListener(this)
        quantity.addTextChangedListener(this)

        spin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                product = parent?.getItemAtPosition(position).toString()
                when (product) {
                    "Oreo" -> img_bestellen.setImageResource(R.drawable.oreo)
                    "Crèpe" -> img_bestellen.setImageResource(R.drawable.crepe)
                    "Cookie" -> img_bestellen.setImageResource(R.drawable.cookie)
                }
                when (product) {
                    "Oreo" -> tv_gewicht.text = "150g"
                    "Crèpe" -> tv_gewicht.text = "250g"
                    "Cookie" -> tv_gewicht.text = "350g"
                }
                tv_naam.text = "Naam: $product"
                tv_aantal.text = "Aantal: " + txt_aantal.text

                tot = if (txt_aantal.text.isEmpty()) {
                    0
                } else {
                    txt_aantal.text.toString().toInt()
                }
                when (product) {
                    "Oreo" -> tv_totaal.text = "Tot: " + "%.2f".format(tot!! * 1.5) + " €"
                    "Crèpe" -> tv_totaal.text = "Tot: " + "%.2f".format(tot!! * 2.1) + " €"
                    "Cookie" -> tv_totaal.text = "Tot: " +"%.2f".format(tot!! * 3.2) + " €"
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.sign_out -> signOut()
            else -> Toast.makeText(this, "$item should be false was pressed", Toast.LENGTH_LONG).show()
        }
        return true
    }

    private fun signOut() {
        mgoogleSingin?.signOut()
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_bestel -> selectRightProduct()
            else -> println("no action Possible")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun selectRightProduct() {

        val builder = AlertDialog.Builder(this@Bestellen)
        if (txt_aantal.text.isEmpty()|| txt_aantal.text.toString().toInt() == 0) {
            builder.setTitle("Aantal kiezen")
            builder.setNegativeButton("Ok"){
                dialog, which -> closeContextMenu()
            }
            builder.setMessage("Gelieve een aantal te kiezen")
            val dialog:AlertDialog = builder.create()
            dialog.show()
        }
        else {
            val toast: Toast = Toast.makeText(this, "bestelling werd gemaakt", Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.LEFT, 0, 0)
            toast.show()
            val quantity = txt_aantal.text.toString().toInt()
            val weight = tv_gewicht.text.toString()
            when (product) {
                "Oreo" -> prepareOrder(product!!, quantity, quantity * 1.5, weight)
                "Crèpe" -> prepareOrder(product!!, quantity, quantity * 2.1, weight)
                "Cookie" -> prepareOrder(product!!, quantity, ("%.2f".format(quantity * 3.2)).toDouble(), weight)
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun prepareOrder(name: String, quantity: Int, price: Double, weight: String) {
        var date = LocalDateTime.now()
        var order = name?.let { Object(it, quantity, weight, price, date.toString()) }
        if (order != null) {
            coll?.document("$date")?.set(order)?.addOnSuccessListener {
            }?.addOnFailureListener { e ->
                Log.w("filure", "Error adding document", e)
            }
        }
    }

    fun click(view: View) {
        intent.action = Intent.ACTION_SEND
        intent.type = "text/plain"
        val i = Intent(this, OverzichtBestellingen::class.java)
        startActivity(i)
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable?) {

        tv_aantal.text = "Aantal: " + txt_aantal.text
        tot = if (txt_aantal.text.isEmpty()) {
           0
        } else {
            txt_aantal.text.toString().toInt()
        }
        when (product) {
            "Oreo" -> tv_totaal.text = "Tot: " + "%.2f".format(tot!! * 1.5) + " €"
            "Crèpe" -> tv_totaal.text = "Tot: " + "%.2f".format(tot!! * 2.1) + " €"
            "Cookie" -> tv_totaal.text = "Tot: " +"%.2f".format(tot!! * 3.2) + " €"
        }

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }


}
