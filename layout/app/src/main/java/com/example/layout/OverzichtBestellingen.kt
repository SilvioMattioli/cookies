package com.example.layout

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.layout.data.Object
import com.example.layout.data.ProductAdapter
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_overzicht_bestellingen.*

class OverzichtBestellingen() : AppCompatActivity() {
    private var adapter: ProductAdapter? = null
    private var list: ArrayList<Object> = arrayListOf()
    private var layoutManager: RecyclerView.LayoutManager? = null
    private val db = FirebaseFirestore.getInstance()
    private var userId: String? = null
    private var coll: CollectionReference? = null
    private var client: GoogleSignInClient?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_overzicht_bestellingen)
        var acct: GoogleSignInAccount = GoogleSignIn.getLastSignedInAccount(this)!!
        userId = acct.id.toString()
        coll = db.collection("Users/$userId/Orders")
        client = GoogleSignIn.getClient(this, GoogleSignInOptions.DEFAULT_SIGN_IN)

        coll?.get()?.addOnSuccessListener { documents ->
            println("inserted")
            for (d in documents) {
                var best = Object(
                    d.data["name"].toString(),
                    d.data["quantity"].toString().toInt(),
                    d.data["weigth"].toString(),
                    d.data["price"].toString().toDouble(),
                    d.data["id"].toString()
                )
                list?.add(list!!.size, best!!)
                println(best.toString())
            }
            println(list?.size)
            if (list != null) {
                layoutManager = LinearLayoutManager(this)
                adapter = ProductAdapter(list!!, this)

                recycleView.layoutManager = layoutManager
                recycleView.adapter = adapter
                adapter!!.notifyDataSetChanged()
            }
        }
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.sign_out -> signOut()
            R.id.go_back -> goBack()
            else -> Toast.makeText(this, "$item should be false was pressed", Toast.LENGTH_LONG).show()
        }
        return true
    }

    private fun signOut() {
        client?.signOut()
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
    }
    private fun goBack(){
        val i = Intent(this, Bestellen::class.java)
        startActivity(i)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }
}
