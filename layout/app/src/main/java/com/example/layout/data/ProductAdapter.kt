package com.example.layout.data

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.layout.R
import com.example.layout.OneProduct
import kotlinx.android.synthetic.main.activity_bestellen.*


class ProductAdapter(private val list: ArrayList<Object>, private val context: Context)
    : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(product: Object) {
            var name: TextView = itemView.findViewById(R.id.tv_naam)
            var quantity: TextView = itemView.findViewById(R.id.tv_aantal)
            var weight: TextView = itemView.findViewById(R.id.tv_gewicht)
            var total: TextView = itemView.findViewById(R.id.tv_totaal)
            var image:ImageView= itemView.findViewById(R.id.imageView)
            name.text = "product: " + product.name
            quantity.text = "Aantal: "+product.quantity.toString()
            weight.text = "tot: " + product.price.toString()+"€"
            total.text = "Gram: " + product.weigth.toString()
            when (product.name) {
                "Oreo" -> image.setImageResource(R.drawable.oreo)
                "Crèpe" -> image.setImageResource(R.drawable.crepe)
                "Cookie" -> image.setImageResource(R.drawable.cookie)
            }
            itemView.setOnClickListener{
                Toast.makeText(context,name.text,Toast.LENGTH_SHORT).show()

                val intent = Intent(context, OneProduct::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("NAME", product.name)
                intent.putExtra("AANTAL", product.quantity.toString())
                intent.putExtra("GEWICHT", product.weigth)
                intent.putExtra("PRICE", product.price)
                intent.putExtra("DOCID", product.id)
                when (product.name) {
                    "Oreo" -> intent.putExtra("IMAGE", R.drawable.oreo)
                    "Crèpe" -> intent.putExtra("IMAGE", R.drawable.crepe)
                    "Cookie" -> intent.putExtra("IMAGE", R.drawable.cookie)
                }
                context.startActivity(intent)
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ProductAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_layout, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return list.size
    }
    override fun onBindViewHolder(holder: ProductAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
}
